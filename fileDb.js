const fs = require('fs');
const path = "./messages";
let data = [];

module.exports = {
	init: () => {
		return new Promise((resolve, reject) => {
			fs.readdir(path, (err, files) => {
				if (files && files.length !== 0) {
					files.forEach(file => {
						fs.readFile(`./messages/${file}`, (err, result) => {
							data.push(JSON.parse(result.toString()));
						});
					});
					resolve();
				} else {
					reject(err);
				}
			})
		});
	},
	getData: () => {
		const fiveMessage = [];
		if(data.length > 5) {
			for(let i = 5; i > 0; i--) {
				fiveMessage.push(data[data.length - i]);
			}
			return fiveMessage;
		} else {
			return data;
		}
	},
	addMessage: (message) => {
		return new Promise((resolve, reject) => {
			message.datetime = new Date().toISOString();
			data.push(message);
			fs.writeFile(`./messages/${Date.now()}.txt`, JSON.stringify(message, null, 2), (error) => {
				if (error) {
					reject(error);
				} else {
					resolve(message);
				}
			});
		});
	},
};