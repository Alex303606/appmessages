const express = require('express');
const app = express();
const port = 8000;
const fileDb = require('./fileDb');
const messages = require('./app/messages');

app.use(express.json());

fileDb.init().then(() => {
	console.log('Database file was loaded!');
	app.use('/messages', messages(fileDb));
	app.listen(port, () => {
		console.log(`Server started on ${port} port`);
	});
}).catch(() => {
	console.log('Database file was empty!');
	app.use('/messages', messages(fileDb));
	app.listen(port, () => {
		console.log(`Server started on ${port} port`);
	});
});